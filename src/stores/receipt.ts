import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { RecieptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { watch } from 'vue'



export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)

  const receipt = ref<Receipt>({
id: 0,
createdDate: new Date(),
totalBefore: 0,
memberDiscound: 0,
total: 0,
receivedAmount: 0,
change: 0,
paymentType: 'cash',
userId: authStore.currentUser.id,
user: authStore.currentUser,
memberId: 0
})
  const receiptItems = ref<RecieptItem[]>([])

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)

    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: RecieptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }

  function removeReceiptItem(receiptItem: RecieptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: RecieptItem) {
    item.unit++
    calReceipt()
  }

  function dec(item: RecieptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }


  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value){
      totalBefore = totalBefore + (item.price * item.unit)
    }
    receipt.value.totalBefore = totalBefore
    if(memberStore.currentMember){
    receipt.value.total = totalBefore * 0.95
    }else{
    receipt.value.total = totalBefore}
  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }

  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscound: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: 0
      }
      memberStore.clear()
  }

  return { 
    receiptItems, receipt, receiptDialog,
    addReceiptItem, removeReceiptItem, inc, dec, calReceipt, showReceiptDialog, clear }
})
