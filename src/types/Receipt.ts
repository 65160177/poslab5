import type { Member } from "./Member";
import type { RecieptItem } from "./ReceiptItem";
import type { User } from "./User";


type Receipt = {
    id: number;
    createdDate: Date;
    totalBefore : number;
    memberDiscound: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: RecieptItem[]
}

export type { Receipt }