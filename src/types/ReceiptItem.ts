import type { Product } from "./Product";

type RecieptItem = {
    id: number;
    name: string;
    price: number;
    unit: number;
    productId: number;
    product?: Product;
}

export { type RecieptItem }